#!/bin/bash
export HISTCONTROL=ignoreboth:erasedups             #ignore if duplicate or if starts with " "
export HISTSIZE=5000                                #default is 500
export TERM="xterm-256color"                        #getting proper colors
export PATH=$PATH:~/Documents/sc:~/.local/bin       #add scripts folder to PATH
#[ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"
export EDITOR=nvim

#if not running interactively, don't do anything
[[ $- != *i* ]] && return

#custom PS1 prompt
PS1='\[\e[31m\][\[\e[33m\]\u\[\e[32m\]@\[\e[34m\]\h \[\e[35m\]\W\[\e[31m\]]\[\e[00m\]\$ '

#ignore case with TAB completion
bind "set completion-ignore-case on"

#source aliases
[ -f $HOME/.config/aliases ] && source "$HOME/.config/aliases"

#better cd
eval "$(zoxide init bash)"

#shopt
shopt -s autocd             #cd into dir without typing in cd
shopt -s cdspell            #autocorrects cd misspellings
shopt -s cmdhist            #save multi-line commands in history as single line
shopt -s dotglob            #includes dotfiles in filename expansions
shopt -s histappend         #do not overwrite history
shopt -s expand_aliases     #expand aliases

#mkdir + cd
mkcd ()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}

#ex = Extractor for all kinds of archives
#usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.xz)        unxz $1      ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
