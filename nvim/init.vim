let mapleader=";"

"Make Ctrl+c work in Visual mode
vnoremap <C-c> "*y

"UI
syntax on
filetype indent on
set number relativenumber
set showcmd
set wildmenu
set lazyredraw
set mouse=a

"Searching
set incsearch
set nohlsearch
"nnoremap <leader><space> :nohlsearch<CR>

"Tabbing
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smartindent
